#ifndef PRINT_H
#define PRINT_H

#define QEMU_SERIAL 0x3f8

int printk(char* fmt, ...);

#endif
