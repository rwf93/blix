#include "io.h"

inline uint8_t inb(uint16_t port) {
	unsigned char data;
	asm volatile("inb %0, %1"
			: "=a"(data)
			: "Nd"(port));
	return data;
}

inline void outb(uint8_t data, uint16_t port) {
	asm volatile(
				"outb %1, %0"
				:
				:"a"(data), "Nd"(port)
			);
}

int init_serial_port(uint16_t serial_bus_id) {
	outb(0x00, serial_bus_id + 1);
	outb(0x80, serial_bus_id + 3);
	outb(0x03, serial_bus_id + 0); // baud (low)
	outb(0x00, serial_bus_id + 1); // baud (high)
	outb(0x03, serial_bus_id + 3);
	outb(0xc7, serial_bus_id + 2);
	outb(0x0b, serial_bus_id + 4);
	outb(0x1e, serial_bus_id + 4);
	outb(0xae, serial_bus_id + 0);

	if(inb(serial_bus_id) != 0xae) {
		return 1;
	}

	outb(0x0f, serial_bus_id + 4);
	return 0;
}

int serial_received(uint16_t port) {
	return inb(port + 5) & 1;
}

int is_transmit_empty(uint16_t port) {
	return inb(port + 5) & 0x20;
}

char read_serial(uint16_t port) {
	while(serial_received(port) == 0);
	return inb(port);
}

void write_serial(uint16_t port, uint8_t data) {
	while(is_transmit_empty(port) == 0);
	outb(data, port);
}
