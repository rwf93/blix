section .multiboot
header_start:
    dd 0xe85250d6
    dd 0
    dd header_end - header_start
    dd 0x100000000 - (0xe85250d6 + 0 + (header_end - header_start))
    dw 0
    dw 0
    dd 8
header_end:

global _START 

section .text
bits 32

extern blix_main

_START:
	; i love u stack
	; pill time
	; where is my stack?
	mov esp, stack_top		
		
	;call setup_paging
	;call enable_features	
	
	; disable previous paging
	mov eax, cr0
	and eax, 01111111111111111111111111111111b
	mov cr0, eax

	call setup_paging
	call setup_features

	lgdt [gdt64.pointer]
	jmp gdt64.code:long_mode

	hlt	

setup_paging:
	mov edi, 0x1000
	mov cr3, edi
	xor eax, eax
	mov ecx, 4096
	rep stosd
	mov edi, cr3
	
	mov DWORD [edi], 0x2003
	add edi, 0x1000
	mov DWORD [edi], 0x3003
	add edi, 0x1000
	mov DWORD [edi], 0x4003
	add edi, 0x1000

	mov ebx, 0x00000003
	mov ecx, 512

.set_entry:
	mov DWORD [edi], ebx
	add ebx, 0x1000
	add edi, 8
	loop .set_entry
	
	ret

setup_features:
	mov eax, cr4
	or eax, 1 << 5
	mov cr4, eax

	mov ecx, 0xc0000080
	rdmsr
	or eax, 1 << 8
	wrmsr

	mov eax, cr0
	or eax, 1 << 31 | 1 << 0
	mov cr0, eax

	mov ecx, 0xc0000080
	rdmsr
	or eax, 1 << 8
	wrmsr

	mov eax, cr0
	or eax, 1 << 31
	mov cr0, eax
	
	ret

section .bss
stack_bottom: resb 16384 
stack_top:

section .rodata

; Access bits
PRESENT        equ 1 << 7
NOT_SYS        equ 1 << 4
EXEC           equ 1 << 3
DC             equ 1 << 2
RW             equ 1 << 1
ACCESSED       equ 1 << 0
 
; Flags bits
GRAN_4K       equ 1 << 7
SZ_32         equ 1 << 6
LONG_MODE     equ 1 << 5

gdt64:
	.null: equ $ - gdt64
		dq 0
	.code: equ $ - gdt64
		dd 0xffff
		db 0
		db PRESENT | NOT_SYS | EXEC | RW
		db GRAN_4K | LONG_MODE | 0xf
		db 0
	.data: equ $ - gdt64
		dd 0xffff
		db 0 
		db PRESENT | NOT_SYS | RW
		db GRAN_4K | SZ_32 | 0xf
		db 0
	.tss: equ $ - gdt64
		dd 0x00000068
		dd 0x00cf8900
	.pointer: 
		dw $ - gdt64 - 1
		dq gdt64

section .text
bits 64

extern blix_main
long_mode:		
	cli

	mov ax, gdt64.data
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax
	mov ss, ax

	mov edi, 0xb8000
	mov rax, 0x1f201f201f201f20
	mov ecx, 500

	rep stosq

	call blix_main
	
	hlt
