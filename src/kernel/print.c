#include "print.h"
#include "io.h"

#include <blix_std/types.h>
#include <blix_std/string.h>
#include <blix_std/va_list.h>

void printk_char(char ch) { 
	if(ch == '\n')
		write_serial(QEMU_SERIAL, '\r');

	write_serial(QEMU_SERIAL, ch);
}

void print(char* str) {
	while(*str != 0) {
		printk_char(*str++); 
	}
}

int printk(char *fmt, ...) {	
	va_list list;
	va_start(list, fmt);
	
	char buf[512];

	for(const char *p = fmt; *p; p++) {
		if(*p == '%' && *(p + 1)) {
			p++;
			switch (*p) {
				case 'i':
					print(itoa(va_arg(list, int), buf, 10));
					break;
				default:
					break;
			}
		} else {
			printk_char(*p);
		}
	}
	
	va_end(list);

	return 0;
}
