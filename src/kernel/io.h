#ifndef IO_H
#define IO_H

#include <blix_std/types.h>

unsigned char inb(uint16_t port);
void outb(uint8_t data, uint16_t port);

// serial
int init_serial_port(uint16_t serial_bus_id);
char read_serial(uint16_t port); 
void write_serial(uint16_t port, uint8_t data);


#endif
